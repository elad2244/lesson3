#include "Vector.h"

using std::cout;
using std::endl;

//////////////////////////
//Functions for part A://
////////////////////////
Vector::Vector(int n)
{
	if (n < 2)
	{
		n = 2;
	}
	this->_elements = new int[n];
	this->_size = 0;
	this->_resizeFactor = n;
	this->_capacity = n;
}

Vector::~Vector()
{
	delete[] _elements;
	_elements = nullptr;
}

int Vector::size() const//return size of vector
{
	return this->_size;
}

int Vector::capacity() const//return capacity of vector
{
	return this->_capacity;
}

int Vector::resizeFactor() const //return vector's resizeFactor
{
	return this->_resizeFactor;
}

bool Vector::empty() const //returns true if size = 0
{
	return this->_size == 0;
}

//////////////////////////
//Functions for part B://
////////////////////////
//Modifiers

void Vector::push_back(const int& val)//adds element at the end of the vector
{
	int * newVector;
	int i = 0;

	if (this->_size < this->_capacity)
	{
		this->_elements[this->_size] = val;
		this->_size++;
	}
	else //this->_size == this->_capacity
	{
		newVector = new int[this->_capacity + this->_resizeFactor];
		this->_capacity += this->_resizeFactor;
		
		for (i = 0; i < this->_size; i++)
		{
			newVector[i] = this->_elements[i];
		}
		
		newVector[this->_size] = val;
		this->_size++;

		Vector::~Vector();//delete ols elements!

		this->_elements = newVector;
	}
}

int Vector::pop_back()//removes and returns the last element of the vector
{
	int errorRet = -9999;

	if (this->empty())
	{
		std::cerr << "The Vector is empty!" << endl;
		return errorRet;
	}
	else
	{
		return this->_elements[this->_size--];
	}
}

void Vector::reserve(int n)//change the capacity
{
	int * newVector;
	int i = 0;

	while (this->_capacity <= n)
	{
		this->_capacity += this->_resizeFactor;
	}

	newVector = new int[this->_capacity];
	
	for (i = 0; i < this->_size; i++)
	{
		newVector[i] = this->_elements[i];
	}

	Vector::~Vector();//delete the old elements array;
	this->_elements = newVector;
}
	
void Vector::resize(int n)//change _size to n, unless n is greater than the vector's capacity
{
	if (n <= this->_capacity)
	{
		this->_size = n;
	}
	else//n > this->_capacity
	{
		this->reserve(n);
		this->_size = n;
	}
}

void Vector::assign(int val)//assigns val to all elemnts
{
	int i = 0;

	for (i = 0; i < this->_size; ++i)
	{
		this->_elements[i] = val;
	}
}

void Vector::resize(int n, const int& val)//same as above, if new elements added their value is val
{
	int i = 0;
	int lastIndex = this->_size;

	this->resize(n);

	for (i = lastIndex; i < this->_size; ++i)
	{
		this->_elements[i] = val;
	}
}

//////////////////////////
//Functions for part C://
////////////////////////
//The big three (d'tor is above)

Vector::Vector(const Vector& other)
{
	*this = other;
	//this->operator=(other);//another way!
}

Vector& Vector::operator=(const Vector& other)
{
	int i = 0;

	if (this == &other) // tries to copy the object to itself
	{
		return *this;
	}
	else
	{
		delete[] this->_elements; // release old memory
		
		this->_capacity = other._capacity;
		this->_resizeFactor = other._resizeFactor;
		this->_size = other._size;
		
		this->_elements = new int[this->_capacity];
		for (i = 0; i < this->_size; i++)
		{
			this->_elements[i] = other._elements[i];
		}
		return *this;
	}
}

//////////////////////////
//Functions for part D://
////////////////////////
//Element Access
int& Vector::operator[](int n) const//n'th element
{
	int i = 0;

	if (n >= this->_size || n < 0)
	{
		std::cerr << "The number " << n << " is bigger then the array size!" << endl;
		return this->_elements[0];
	}
	else
	{
		return this->_elements[n];
	}
}